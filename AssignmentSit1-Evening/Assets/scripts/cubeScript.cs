﻿using UnityEngine;
using System.Collections;

public class cubeScript : MonoBehaviour {

	public int rowIndex;
	public int columnIndex;

    Color currColor;
	//string currentTag;
    public bool clicked = false;
    public bool animate = false;
    void Start()
    {
        currColor = GetComponent<SpriteRenderer>().color;
        StartCoroutine(animateCube());
    }


    IEnumerator animateCube()
    {
        while (true)
        {
            if (animate)
            { 
                this.transform.rotation *= Quaternion.Euler(0f, 0f, 5f);
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                yield return null;
            }
        }
        
    }



    void OnMouseDown()
    {
        if (!clicked)
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            clicked = true;
            animate = false;
			this.tag = "clicked";
        }
        else
        {
            GetComponent<SpriteRenderer>().color = currColor;
            clicked = false;
            animate = true;
			this.tag="Untagged";
        }
    }

    public void resetColor()
    {
        GetComponent<SpriteRenderer>().color = currColor;
        clicked = false;
    }



}
