﻿using UnityEngine;
using UnityEngine.UI;

using System.Linq;
using System.Collections;
using System.Collections.Generic;


public class spawnerController : MonoBehaviour {

	GameObject cube;

	public List<GameObject> allCubes;



    



	void generateGrid()
	{
        
		GameObject temp;
		allCubes = new List<GameObject> ();
		for (int r=0; r<rows; r++) {
            
			for (int col=0; col<columns; col++) {
				temp = (GameObject)Instantiate (cube,new Vector3(col,r),Quaternion.identity);
				temp.transform.parent = this.transform;
				temp.GetComponent<cubeScript>().columnIndex = col;
				temp.GetComponent<cubeScript>().rowIndex = r;
				temp.name = col + " " + r;
				allCubes.Add(temp);
               // if ((r >= col) || (r <= ((numberofsteps - 1) - col)))
               // {
                    temp.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                    
               // }
            }
            

        }
      
          
            
        

	}


	void HideAllCubes()
	{

		foreach (Transform t in transform) {
			//Debug.Log(t.name);
			t.gameObject.GetComponent<SpriteRenderer>().enabled=false;
		}

	}

	IEnumerator AnimateGrid()
	{
		
		while (true) {
			HideAllCubes ();
			
			foreach (Transform t in transform) {
				GameObject temp = t.gameObject;
				temp.GetComponent<SpriteRenderer> ().enabled = true;
				temp.GetComponent<SpriteRenderer> ().color = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
				yield return new WaitForSeconds (0.2f);
			}
				//yield return null;
		}

	}




	/*
	void rotateMe()
	{
        
			foreach (Transform t in transform) {
            //Debug.Log(t.name);
                t.GetComponent<cubeScript>().animate = true;
 

        }
			
            
		
	}*/


	public void resetCubeColor()
	{
		if (allCubes.All (obj => obj.GetComponent<Renderer> ().material.color == Color.red)) {
			foreach (GameObject cube in allCubes) {
				cube.GetComponent<cubeScript> ().resetColor ();
			}
		}
	}


	public void scaleStairs()
	{
		float scaleInX = 0f;
		float scaleInY = 0f;
		
		
		//Debug.Log (allCubes.Count);

		//Debug.Log (margin * 2);
		//calculate Y scale factor
		//scaleInY =  (Camera.main.orthographicSize * 2) / (numberofsteps+(margin*2));

		//scaleInX =  (Camera.main.orthographicSize * Camera.main.aspect * 2) / (numberofsteps+(margin*2));

		scaleInY =  (Camera.main.orthographicSize * 2) / (rows);
		scaleInX =  (Camera.main.orthographicSize * Camera.main.aspect * 2) / (columns);

        //z value is 1 so the boxes are not shown @ 0 width
		this.transform.localScale = new Vector3 (scaleInX, scaleInY,1f);
		//to change the offset from the bottom left corner given the different scale - basically cater for the pivot point
		this.transform.position += new Vector3 (scaleInX / 2, scaleInY / 2);
		//this.transform.position += new Vector3 (scaleInX*margin, scaleInY*margin);
	}


	float rows;
	float columns;

	void resetStartPoint()
	{
		float cameraSize = Camera.main.orthographicSize;
		transform.position = new Vector3 (-cameraSize * Camera.main.aspect, -cameraSize);
		scaleStairs ();
	}

	// Use this for initialization
	void Start () {
		rows = uiScript.rows;
		columns = uiScript.columns;
		//generateChessboard ();

		cube = Resources.Load ("Square") as GameObject;

		generateGrid ();

		//puts the parent object in the top left corner.
		resetStartPoint();

		StartCoroutine (AnimateGrid ());

		//rotateMe ();
	
	}
	

}
