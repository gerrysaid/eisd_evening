﻿using UnityEngine;
using System.Collections;

public class mouseController : MonoBehaviour {
	GameObject triangle;
	GameObject shot;
	GameObject point;

	public GameObject bullet;


	// Use this for initialization
	void Start () {
		triangle = GameObject.Find ("Triangle");
		point = GameObject.Find ("Point");
	}
	
	// Update is called once per frame
	void Update () {
		//read the mouse position
		Vector3 mousePosition = Input.mousePosition;

		//to translate the mouse position to world coordinates
		Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePosition);

		//to extract the x and y values without using the Z value;
		transform.position = new Vector3 (mouseWorldPosition.x, mouseWorldPosition.y);

		//this is the difference between the box position and the triangle position
		Vector3 difference = transform.position - triangle.transform.position;

		//calculate the tangent and convert the value (in radians) to degrees
		float zAngle = Mathf.Atan2 (difference.y, difference.x) * Mathf.Rad2Deg; 

		//set the rotation of the triangle to 0 in x, 0 in y and the angle we want in z
		triangle.transform.rotation = Quaternion.Euler (new Vector3 (0f, 0f, zAngle));

		triangle.transform.Translate (Vector3.right * Input.GetAxis ("Vertical") * 10f * Time.deltaTime);

		triangle.transform.Translate (Vector3.down * Input.GetAxis ("Horizontal") * 10f * Time.deltaTime);

		//when you left click shoot a bullet forward from the triangle....
		if (Input.GetMouseButtonDown (0)) {
			if (shot == null)
				shot = Instantiate (bullet, point.transform.position, point.transform.rotation) as GameObject;
		}

		if (shot != null) {
			shot.transform.Translate (Vector3.up * 15f * Time.deltaTime);

			//assuming a square screen with an orthographic size of 5
			if ((shot.transform.position.y > 5) || (shot.transform.position.y < -5) || (shot.transform.position.x > 5) || (shot.transform.position.x < -5)) {
				Destroy (shot);
			}
		}
	}
}
