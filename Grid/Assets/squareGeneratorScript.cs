﻿using UnityEngine;
using System.Collections;

public class squareGeneratorScript : MonoBehaviour {

	public GameObject squareToGenerate;

	//y-axis
	public int rows = 3;
	//x-axis
	public int columns = 3;




	IEnumerator animateGrid()
	{
		for (int r = 0; r < rows; r++) {
			//loop that runs for 3 times for the columns
			for (int c = 0; c < columns; c++) {
				GameObject.Find("Column: " + c + " Row: " + r).GetComponent<SpriteRenderer>().color = Color.red;
				yield return new WaitForSeconds (1f);
				GameObject.Find("Column: " + c + " Row: " + r).GetComponent<SpriteRenderer>().color = Color.white;
				yield return new WaitForSeconds (1f);
			}
		}
		yield return null;
	}
	//builds a grid starting from the position of the parent object
	void generateGrid(){
		//loop runs for 20 times for the rows
		for (int r = 0; r < rows; r++) {
			//loop that runs for 20 times for the columns
			for (int c = 0; c < columns; c++) {
				//generate the boxes at z=1 in front of the camera
				GameObject temp = Instantiate(squareToGenerate,new Vector3(c,r,1f),Quaternion.identity) as GameObject;
				//make the parent the square generator
				temp.transform.parent = this.transform;
				//set the name to Column: 0 and Row: 0 for each box
				temp.name = "Column: " + c + " Row: " + r;
			}

		}
	}

	void scaleGridY(){
		float totalHeight = 0f;
		//totalHeight = Camera.main.orthographicSize;
		//assuming a camera.main.orthographicsize of 5
		totalHeight = 10f;
		float boxHeight = 0f;
		//the height of each box
		boxHeight = totalHeight / rows;
		//move the parent object to the bottom of the screen
		transform.position = new Vector3 (0f, -5f + (boxHeight / 2));
		//scale the object, leave Z 1
		transform.localScale = new Vector3(1f,boxHeight,1f);
	}

	void scaleGridX()
	{
		float totalWidth = 0f;
		//multiply the total height * the aspect ratio
		totalWidth = (Camera.main.orthographicSize * Camera.main.aspect)*2;

		float boxWidth = 0f;

		boxWidth = totalWidth / columns;

		//keep the y position
		float yPosition = transform.position.y;

		float xPosition = -(totalWidth/2) + (boxWidth / 2);
		//keep the Y scale
		float yScale = transform.localScale.y;

		transform.position = new Vector3 (xPosition, yPosition);

		transform.localScale = new Vector3 (boxWidth, yScale, 1f);


	}

	//needs to be linked to the button
	public void resetButtonPressed(){
		for (int i = 0; i < transform.childCount; i++) {
			GameObject box = transform.GetChild (i).gameObject;
			box.GetComponent<SpriteRenderer> ().color = Color.white;
		}
	}

	//needs to be linked to another button in this case
	public void highlightMiddleColumnPressed(){
		for (int i = 0; i < rows; i++) {
			GameObject.Find("Column: " + columns/2 + " Row: " + i).GetComponent<SpriteRenderer>().color = Color.red;
		}

	}

	//needs to be linked to another button
	public void highlightMiddleRowPressed()
	{
		for (int i = 0; i < columns; i++) {
			GameObject.Find("Column: " + i + " Row: " + rows/2).GetComponent<SpriteRenderer>().color = Color.red;
		}

	}

	public void highlightDiagonalLineAcross1()
	{
		for (int i = 0; i < columns; i++) {
			GameObject.Find("Column: " + i + " Row: " + i).GetComponent<SpriteRenderer>().color = Color.red;
		}
	}

	public void highlightDiagonalLineAcross2()
	{
		for (int i = 0; i < columns; i++) {
			int rowValue = (columns-1)-i;
			GameObject.Find("Column: " + i + " Row: " + rowValue).GetComponent<SpriteRenderer>().color = Color.red;
		}
	}



	// Use this for initialization. Happens when I press play
	void Start () {
		int counter = 0;
		generateGrid ();
		scaleGridY ();
		scaleGridX ();



		//StartCoroutine (animateGrid ());


		//create 3 boxes on top of each other, and group them together
		//into one object
		/*
		do {
			counter++;
			GameObject temp = Instantiate(squareToGenerate,new Vector3(0f,counter,1f),Quaternion.identity) as GameObject;
			temp.transform.parent = this.transform;
			temp.name = counter.ToString();
		} while(counter < numberOfSquares);
		//StartCoroutine (animatedBoxes ());
		StartCoroutine(trafficLights());
		*/
	}

	IEnumerator trafficLights(){
		//sequence of traffic lights
		//switch on red, then switch on red and amber, finally switch off red and amber and switch on green
		GameObject redLight = GameObject.Find("3");
		GameObject yellowLight = GameObject.Find ("2");
		GameObject greenLight = GameObject.Find ("1");

		redLight.GetComponent<SpriteRenderer> ().color = Color.red;
		yield return new WaitForSeconds (1f);
		yellowLight.GetComponent<SpriteRenderer> ().color = Color.yellow;
		yield return new WaitForSeconds (1f);
		redLight.GetComponent<SpriteRenderer> ().color = Color.white;
		yellowLight.GetComponent<SpriteRenderer> ().color = Color.white;
		greenLight.GetComponent<SpriteRenderer>().color = Color.green;
		yield return new WaitForSeconds (1f);
		yield return null;
	}


	IEnumerator animatedBoxes()
	{
		int counter = 0;
		//animation is going to go on forever
		while (true) {
			if (counter < 3) {
				counter++;
			} else {
				counter = 1;
			}
			//find the box you want
			GameObject.Find(counter.ToString()).GetComponent<SpriteRenderer>().color = Color.red;
			yield return new WaitForSeconds (1f);
			GameObject.Find(counter.ToString()).GetComponent<SpriteRenderer>().color = Color.white;
			yield return new WaitForSeconds (1f);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
