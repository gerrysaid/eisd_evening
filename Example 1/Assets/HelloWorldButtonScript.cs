﻿using UnityEngine;
//to use the Text component
using UnityEngine.UI;
using System.Collections;

public class HelloWorldButtonScript : MonoBehaviour {

	//this method is going to be triggered when the button is pressed
	public void helloWorldButtonPressed()
	{
		string output = "Hello, my name is Gerry";
		//set the text to the value I want here
		GameObject.Find ("HelloWorldText").GetComponent<Text> ().text = output;
	}

	public void resetButtonPressed()
	{
		string output = "Hello world";
		//set the text to the value I want here
		GameObject.Find ("HelloWorldText").GetComponent<Text> ().text = output;
		GameObject.Find ("MySquare").GetComponent<SpriteRenderer> ().color = Color.white;

		//reset square position to its starting position
		Vector3 squareStartingPosition = GameObject.Find("MySquare").GetComponent<SquareController>().startingPosition;
		GameObject.Find ("MySquare").transform.position = squareStartingPosition;

	}

	public void redButtonPressed()
	{
		Debug.Log ("red button");
		GameObject.Find ("MySquare").GetComponent<SpriteRenderer> ().color = Color.red;
	}

	public void greenButtonPressed()
	{
		Debug.Log ("green button");
		GameObject.Find ("MySquare").GetComponent<SpriteRenderer> ().color = Color.green;
	}

	public void yellowButtonPressed()
	{
		Debug.Log ("yellow button");
		GameObject.Find ("MySquare").GetComponent<SpriteRenderer> ().color = Color.yellow;
	}

}
