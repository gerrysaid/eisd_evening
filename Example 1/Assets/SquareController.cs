﻿using UnityEngine;
using System.Collections;

public class SquareController : MonoBehaviour {

	//this will be filled with the square's initial position
	public Vector3 startingPosition;

	// Use this for initialization
	void Start () {
		//this refers to the box that the square is attached to.
		startingPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//horizontal movement 10 is the speed of the horizontal movement
		transform.Translate(new Vector3(1f,0f,0f) * 10f * Time.deltaTime * Input.GetAxis("Horizontal"));

		//vertical movement 10 is the speed of the vertical movement
		transform.Translate(new Vector3(0f,1f,0f) * 10f * Time.deltaTime * Input.GetAxis("Vertical"));

	
	}
}
