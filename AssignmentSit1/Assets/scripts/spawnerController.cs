﻿using UnityEngine;
using UnityEngine.UI;

using System.Linq;
using System.Collections;
using System.Collections.Generic;


public class spawnerController : MonoBehaviour {

	GameObject cube;

	public List<GameObject> allCubes;



    



	void generateGrid()
	{
		GameObject temp;
		allCubes = new List<GameObject> ();
		for (int r=0; r<numberofsteps; r++) {
			for (int col=0; col<numberofsteps; col++) {
				temp = (GameObject)Instantiate (cube,new Vector3(col,r),Quaternion.identity);
				temp.transform.parent = this.transform;
				temp.GetComponent<cubeScript>().columnIndex = col;
				temp.GetComponent<cubeScript>().rowIndex = r;
				temp.name = col + " " + r;
				allCubes.Add(temp);
				if ((r == col) || (r==((numberofsteps-1)-col))) {
					temp.GetComponent<SpriteRenderer> ().color = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
				}


			}

		}

	}



	IEnumerator rotateMe()
	{
		float rotation = 0f;
        int counter = 0;
		while (true) {
			foreach (Transform t in transform) {
				Debug.Log(t.name);
				t.rotation *= Quaternion.Euler (0f, 0f, 45f);	

			}
			yield return new WaitForSeconds (1f);
            
		}
	}


	public void resetCubeColor()
	{
		if (allCubes.All (obj => obj.GetComponent<Renderer> ().material.color == Color.red)) {
			foreach (GameObject cube in allCubes) {
				cube.GetComponent<cubeScript> ().resetColor ();
			}
		}
	}


	public void scaleStairs()
	{
		float scaleInX = 0f;
		float scaleInY = 0f;
		
		
		Debug.Log (allCubes.Count);

		Debug.Log (margin * 2);
		//calculate Y scale factor
		scaleInY =  (Camera.main.orthographicSize * 2) / (numberofsteps+(margin*2));

		scaleInX =  (Camera.main.orthographicSize * Camera.main.aspect * 2) / (numberofsteps+(margin*2));



		this.transform.localScale = new Vector3 (scaleInX, scaleInY,1f);
		//to change the offset from the bottom left corner given the different scale
		this.transform.position += new Vector3 (scaleInX / 2, scaleInY / 2);
		this.transform.position += new Vector3 (scaleInX*margin, scaleInY*margin);
	}


	float numberofsteps;
	float margin;

	// Use this for initialization
	void Start () {
		numberofsteps = uiScript.numberofsteps;
		margin = uiScript.margin;
		//generateChessboard ();

		cube = Resources.Load ("Square") as GameObject;

		generateGrid ();
		float cameraSize = Camera.main.orthographicSize;
		//puts the parent object in the top left corner.

		transform.position = new Vector3 (-cameraSize * Camera.main.aspect, -cameraSize);
		scaleStairs ();
		StartCoroutine (rotateMe ());
	
	}
	

}
