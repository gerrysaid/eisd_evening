﻿using UnityEngine;
using System.Collections;

public class cubeScript : MonoBehaviour {

	public int rowIndex;
	public int columnIndex;

    Color currColor;
	//string currentTag;
    bool clicked = false;

    void Start()
    {
        currColor = GetComponent<Renderer>().material.color;


    }



    void OnMouseDown()
    {
        if (!clicked)
        {
            GetComponent<Renderer>().material.color = Color.red;
            clicked = true;
			this.tag = "clicked";
//            GameObject.Find("Spawner").GetComponent<spawnerController>().countCubes();
        }
        else
        {
            GetComponent<Renderer>().material.color = currColor;
            clicked = false;
			this.tag="Untagged";
        }
    }

    public void resetColor()
    {
        GetComponent<Renderer>().material.color = currColor;
        clicked = false;
    }



	public bool aboveRed()
	{




		int rowToFindDown = rowIndex-1;
		bool below = false;
		if ((GameObject.Find(columnIndex + " " + rowToFindDown) != null) && (GameObject.Find(columnIndex + " " + rowToFindDown).GetComponent<Renderer>().material.color == Color.red))
		{
			below = true;
		}
		return below;

	}


	public bool leftRed()
	{
		int colToFindLeft = columnIndex-1;
		bool left = false;

		if ((GameObject.Find(colToFindLeft + " " + rowIndex) != null) && (GameObject.Find(colToFindLeft + " " + rowIndex).GetComponent<Renderer>().material.color == Color.red))
		{
			left = true;
		}

		return left;


	}




    public bool IsAdjacentToRed()
    {
        
        
        //check 1 up
        int rowToFindUp = rowIndex+1;
        int colToFindRight = columnIndex+1;

        int rowToFindDown = rowIndex-1;
        int colToFindLeft = columnIndex-1;

        bool above = false;
        bool below = false;
        bool diagupr = false;
        bool diagdnr = false;
        bool diagupl = false;
        bool diagdnl = false;

        bool left = false;
        bool right = false;

        if (this.GetComponent<Renderer>().material.color == Color.red) { 
        //right
        if ((GameObject.Find(colToFindRight +" "+rowIndex)!=null) && (GameObject.Find(colToFindRight +" "+rowIndex).GetComponent<Renderer>().material.color == Color.red))
        {
            right = true;
        }
        //left
        if ((GameObject.Find(colToFindLeft + " " + rowIndex) != null) && (GameObject.Find(colToFindLeft + " " + rowIndex).GetComponent<Renderer>().material.color == Color.red))
        {
            left = true;
        }

        //diagonal right up
        if ((GameObject.Find(colToFindRight + " " + rowToFindUp) != null) && (GameObject.Find(colToFindRight + " " + rowToFindUp).GetComponent<Renderer>().material.color == Color.red))
        {
            diagupr = true;
        }


        //diagonal left up
        if ((GameObject.Find(colToFindLeft + " " + rowToFindUp) != null) && (GameObject.Find(colToFindLeft + " " + rowToFindUp).GetComponent<Renderer>().material.color == Color.red))
        {
				diagupl = true;
        }

        //diagonal right down
        if ((GameObject.Find(colToFindRight + " " + rowToFindDown) != null) && (GameObject.Find(colToFindRight + " " + rowToFindDown).GetComponent<Renderer>().material.color == Color.red))
        {
				diagdnr = true;
        }

        //diagonal left down
        if ((GameObject.Find(colToFindLeft + " " + rowToFindDown) != null) && (GameObject.Find(colToFindLeft + " " + rowToFindDown).GetComponent<Renderer>().material.color == Color.red))
        {
				diagdnl = true;
        }

        //down
        if ((GameObject.Find(columnIndex + " " + rowToFindDown) != null) && (GameObject.Find(columnIndex + " " + rowToFindDown).GetComponent<Renderer>().material.color == Color.red))
        {
				below = true;
        }

        //up
        if ((GameObject.Find(columnIndex + " " + rowToFindUp) != null) && (GameObject.Find(columnIndex + " " + rowToFindUp).GetComponent<Renderer>().material.color == Color.red))
        {
				above = true;
        }


			if (right && diagupr) return false;
			if (left && diagupl) return false;
			if (right && diagdnr) return false;
			if (left && diagdnl) return false;
			if (above && diagupl) return false;
			if (above && diagupr) return false;
			if (below && diagdnl) return false;
			if (below && diagdnr) return false;
			if (left) return true;
			if (right) return true;
			if (diagupr) return true;
			if (diagupl) return true;
			if (diagdnr) return true;
			if (diagdnl) return true;
			if (above) return true;
			if (below) return true;

        }
        //check 1 diagonal
 
        return false;

    }
}
