﻿using UnityEngine;
using System.Collections;

public class chessboardGenerator : MonoBehaviour {


	public GameObject chessBlock;

	public int rows=0;
	public int columns=0;


	void generateGrid()
	{
		for (int r = 0; r < rows; r++) {
			for (int c = 0; c < columns; c++) {
				GameObject tempBox = Instantiate (chessBlock, new Vector3 (c, r), transform.rotation) as GameObject;
				tempBox.transform.SetParent (this.transform);
				tempBox.name = "Box_at_column " + c + "row "+r;
				if ((c+r)%2 == 0) {
					tempBox.GetComponent<SpriteRenderer> ().color = Color.black;
				} else {
					tempBox.GetComponent<SpriteRenderer> ().color = Color.white;
				}


			}
		}


	}

	void scaleGrid()
	{
		float totalHeight = Camera.main.orthographicSize * 2;
		float totalWidth = Camera.main.orthographicSize * Camera.main.aspect * 2;

		float scaleY = totalHeight / rows;
		float scaleX = totalWidth / columns;

		transform.localScale = new Vector3 (scaleX, scaleY,1f);


	}


	void bottomLeft()
	{
		float leftMarginX = -Camera.main.orthographicSize * Camera.main.aspect;
		float bottomMarginY = -Camera.main.orthographicSize;

		float offsetX = transform.localScale.x/2f;
		float offsetY = transform.localScale.y/2f;

		transform.position = new Vector3 (leftMarginX, bottomMarginY) + new Vector3 (offsetX, offsetY);

	}


	// Use this for initialization
	void Start () {
		generateGrid ();
		scaleGrid ();
		bottomLeft ();
	}
	
	// Update is called once per frame
	void Update () {
		scaleGrid ();
		bottomLeft ();
	
	}
}
