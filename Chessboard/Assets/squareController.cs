﻿using UnityEngine;
using System.Collections;

public class squareController : MonoBehaviour {
	bool clicked = false;
	Color currentColor;


	void Start(){
		currentColor = GetComponent<SpriteRenderer> ().color;
	}

	void OnMouseDown(){
		
		if (clicked == false) {
			GetComponent<SpriteRenderer> ().color = Color.red;
			clicked = true;
		} else {
			GetComponent<SpriteRenderer> ().color = currentColor;
			clicked = false;
		}

	}


}
