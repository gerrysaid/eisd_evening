﻿using UnityEngine;
using System.Collections;

public class greenCubeController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	//happens when the green cube hits something else
	void OnCollisionEnter(Collision theCollision)
	{
		Debug.Log (theCollision.gameObject.name);
		if (theCollision.gameObject.name == "Plane") {
			//if the green cube enters the plane, push it up with a force of 1
			GetComponent<Rigidbody> ().AddForce (new Vector3 (0f, 20f, 0f),ForceMode.Impulse);

		}

	}

	// Update is called once per frame
	void Update () {
	
	}
}
