﻿using UnityEngine;
using System.Collections;

public class animatedSquareController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	bool spacePressed = false;

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Space)) {
			if ((spacePressed == false)) {
				GetComponent<Animator> ().SetTrigger ("StartJumping");
				spacePressed = true;
			} else {
				GetComponent<Animator> ().SetTrigger ("StopJumping");
				spacePressed = false;
			}
		}
	
	}
}
