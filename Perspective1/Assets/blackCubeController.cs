﻿using UnityEngine;
using System.Collections;

public class blackCubeController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		transform.Translate (Vector3.forward * 10f * Input.GetAxis ("Vertical") * Time.deltaTime);

		transform.Rotate (Vector3.up * 30f * Input.GetAxis ("Horizontal") * Time.deltaTime);
	
	}
}
