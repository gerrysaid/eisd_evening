﻿using UnityEngine;
using System.Collections;

public class cubeController : MonoBehaviour {
	bool rotateinx,rotateiny,rotateinz = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.A)) {
			rotateinx = !rotateinx;
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			rotateiny = !rotateiny;
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			rotateinz = !rotateinz;
		}

		if (rotateinx)
			transform.Rotate (Vector3.left * 10f * Time.deltaTime);

		if (rotateiny)
			transform.Rotate (Vector3.up * 10f * Time.deltaTime);

		if (rotateinz)
			transform.Rotate (Vector3.forward * 10f * Time.deltaTime);

		if (Input.GetKeyDown (KeyCode.Q)) {
			//we need the world coordinates at the top right of the screen
			Vector3 topLeft = Camera.main.ViewportToWorldPoint(new Vector3(0f,1f,10f));
			Debug.Log (topLeft);
			transform.position = topLeft;
			transform.position += new Vector3 (0.5f, -0.5f, 0.5f);
		}

		if (Input.GetKeyDown (KeyCode.W)) {
			//we need the world coordinates at the top right of the screen
			Vector3 topRight = Camera.main.ViewportToWorldPoint(new Vector3(1f,1f,10f));
			Debug.Log (topRight);
			transform.position = topRight;
			transform.position += new Vector3 (-0.5f, -0.5f, 0.5f);
		}

		if (Input.GetKeyDown (KeyCode.Z)) {
			//we need the world coordinates at the top right of the screen
			Vector3 bottomLeft = Camera.main.ViewportToWorldPoint(new Vector3(0f,0f,10f));
			Debug.Log (bottomLeft);
			transform.position = bottomLeft;
			transform.position += new Vector3 (0.5f, 0.5f, 0.5f);
		}

		if (Input.GetKeyDown (KeyCode.X)) {
			//we need the world coordinates at the top right of the screen
			Vector3 bottomRight = Camera.main.ViewportToWorldPoint(new Vector3(1f,0f,10f));
			Debug.Log (bottomRight);
			transform.position = bottomRight;
			transform.position += new Vector3 (-0.5f, 0.5f, 0.5f);
		}


	
	}
}
