﻿using UnityEngine;
using System.Collections;

public class trianglebulletController : MonoBehaviour {

	squareController squareScript;

	// Use this for initialization
	void Start () {
		squareScript = GameObject.Find ("Square").GetComponent<squareController> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3 (0f, 1f) * 5f * Time.deltaTime);
		if (transform.position.y > (squareScript.getLimitY ()-transform.localScale.y/2f)) {
			Destroy (gameObject);
		}
	}
}
