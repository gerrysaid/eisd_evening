﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class squareController : MonoBehaviour {

	public float speed = 10f;
	public GameObject anything,circlebullet;

	float limity,limitx=0f;

	float squarelimitx,squarelimity=0f;


	int ammoLimit = 20;
	int shotsFired = 0;

	public float getLimitX()
	{
		return limitx;
	}

	public float getLimitY()
	{
		return limity;
	}


	void updateAmmo(){
		string ammoText = shotsFired + "/" + ammoLimit;
		GameObject.Find ("AmmoLeft").GetComponent<Text> ().text = ammoText;
	}


	// Use this for initialization
	void Start () {
		limity = Camera.main.orthographicSize;
		limitx = Camera.main.orthographicSize * Camera.main.aspect;

		squarelimity = limity - (transform.localScale.y / 2f);
		squarelimitx = limitx - (transform.localScale.x / 2f);

		updateAmmo ();

	}
	
	// Update is called once per frame
	void Update () {

		//vertical movement

		transform.Translate (new Vector3 (0f, 1f) * speed * Input.GetAxis ("Vertical") * Time.deltaTime);

		transform.Translate (new Vector3 (1f, 0f) * speed * Input.GetAxis ("Horizontal") * Time.deltaTime);

		//the y coordinate of the box
		//Debug.Log(transform.position.y);

		transform.position = new Vector3(Mathf.Clamp(transform.position.x,-squarelimitx,squarelimitx),
			Mathf.Clamp (transform.position.y, -squarelimity, squarelimity));


		//pressed space bar
		if (Input.GetKeyDown (KeyCode.Space)) {
			
			updateAmmo ();

			if (shotsFired < ammoLimit) {
				Vector3 shootingPosition = new Vector3 (transform.position.x,
					                          transform.position.y + transform.localScale.y / 2);


				Instantiate (anything, shootingPosition, transform.rotation);
				shotsFired++;
			}
		}

	
	}
}
