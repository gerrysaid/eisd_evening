﻿using UnityEngine;
using System.Collections;

public class asteroidController : MonoBehaviour {

	float startTime = 0f;
	float directionx = 1f;
	float directiony = 1f;

	triangleController spaceshipScript;

	// Use this for initialization
	void Start () {
		startTime = Time.time;

		spaceshipScript = GameObject.Find ("Spaceship").GetComponent<triangleController> ();
	}
	
	// Update is called once per frame
	void Update () {
		//one second has elapsed

		transform.Translate (Vector3.left * 2f * directionx * Time.deltaTime,Space.World);

		transform.Translate (Vector3.up * 2f * directiony * Time.deltaTime,Space.World);

		transform.Rotate (new Vector3 (0f, 0f, 1f) * 20f * Time.deltaTime);

		float timegapx = Random.Range (4f, 10f);
		float timegapy = Random.Range (4f, 10f);
		//Debug.Log (timegap);
		if (Time.time - startTime > timegapx) {
			Debug.Log ("a"+(Time.time - startTime));
			directionx = directionx * -1;
			startTime = Time.time;
		}


		if (Time.time - startTime > timegapy) {
			Debug.Log ("a"+(Time.time - startTime));
			directiony = directiony * -1;
			startTime = Time.time;
		}


		//using the margins script of the triangle
		transform.position = spaceshipScript.margins (this.transform.position);

		//every second, the x direction is reversed.
	
	}
}
