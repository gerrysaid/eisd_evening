﻿using UnityEngine;
using System.Collections;

public class triangleController : MonoBehaviour {
	public GameObject bulletReference;

	public GameObject bigAsteroid;

	public int numberOfAsteroids;


	IEnumerator generateAsteroids()
	{
		int asteroidCount = 0;
		while (asteroidCount < numberOfAsteroids) {
			Vector3 randomPosition = new Vector3 (Random.Range (-xmargin, xmargin), Random.Range (-ymargin, ymargin));

			GameObject tempAsteroid = Instantiate (bigAsteroid, randomPosition, Quaternion.identity) as GameObject;

			tempAsteroid.AddComponent<asteroidController> ();
			asteroidCount++;
			yield return new WaitForSeconds(Random.Range(1f,5f));
		}
		yield return null;
	}

	
	float xmargin;
	float ymargin;

	// Use this for initialization
	void Start () {
		xmargin = Camera.main.orthographicSize * Camera.main.aspect;
		ymargin = Camera.main.orthographicSize;
		StartCoroutine (generateAsteroids());
	}


	public Vector3 margins(Vector3 positionToCheck)
	{
		Vector3 returnedPosition = positionToCheck;

		if (positionToCheck.y > ymargin) {
			 returnedPosition = new Vector3 (positionToCheck.x, -ymargin);	
		}
		if (positionToCheck.y < -Camera.main.orthographicSize) {
			returnedPosition = new Vector3 (positionToCheck.x, ymargin);	
		}

		if (positionToCheck.x < -xmargin) {
			returnedPosition = new Vector3 (xmargin, positionToCheck.y);	
		}

		if (positionToCheck.x > xmargin) {
			returnedPosition = new Vector3 (-xmargin, positionToCheck.y);	
		}

		return returnedPosition;
	}

	
	// Update is called once per frame
	void Update () {
		//move in the spaceship's LOCAL y axis

		if (Input.GetAxis("Vertical")>0)
			transform.Translate(new Vector3(0f,1f) * 10f * Input.GetAxis("Vertical") * Time.deltaTime);

		transform.Rotate (new Vector3 (0f, 0f, -1f) * 40f * Input.GetAxis ("Horizontal") * Time.deltaTime);

		//Debug.Log (transform.position.x + "-" + transform.position.y);

		transform.position = margins (transform.position);
		//screen edge values
		//in Y
		//Camera.main.orthographicSize<->-Camera.main.orthographicSize

		//in X
		//Camera.main.orthographicSize * Camera.main.aspect<->-Camera.main.orthographicSize * Camera.main.aspect
		if (Input.GetKeyDown (KeyCode.Space)) {
			//get the first child of the triangle object
			Vector3 shootingPosition = transform.GetChild (0).position;

			GameObject tempBullet = Instantiate (bulletReference, shootingPosition, transform.rotation) as GameObject;

			tempBullet.GetComponent<SpriteRenderer> ().color = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));


		}

	}
}
