﻿using UnityEngine;
using System.Collections;

public class bulletController : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.up * 15f * Time.deltaTime);
	}

	//destroy the bullet when it is no longer visible
	void OnBecameInvisible()
	{
		Destroy (this.gameObject);
	}

}
