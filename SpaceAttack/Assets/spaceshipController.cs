﻿using UnityEngine;
//to be able to use text components
using UnityEngine.UI;

using System.Collections;

public class spaceshipController : MonoBehaviour {
	public int speed;

	public GameObject bullet;

	public GameObject ammoBox;

	public GameObject alien;

	int shotsfired = 0;
	int totalbullets = 20;

	public int score=0;

	public int lives=10;


	float limitx,limity;

	IEnumerator createAmmoBox()
	{
		while (true) {
			yield return new WaitForSeconds (3f);
			Instantiate (ammoBox,
				new Vector3 (Random.Range (-limitx, limitx), 5f), transform.rotation);
		}
		yield return null;
	}


	IEnumerator createAlien()
	{
		while (true) {
			yield return new WaitForSeconds (2f);
			Instantiate (alien,
				new Vector3 (Random.Range (-limitx, limitx), 5f), transform.rotation);
		}
		yield return null;
	}


	// Use this for initialization
	void Start () {
		Debug.Log ("Welcome to space attack!");	

		limitx = (Camera.main.orthographicSize * Camera.main.aspect)-(transform.localScale.x/2);
		limity = Camera.main.orthographicSize-(transform.localScale.y/2);
		StartCoroutine (createAmmoBox ());
		StartCoroutine (createAlien ());
		UpdateText ();
	

	}


	public void UpdateText(){
		GameObject.Find ("AmmoCaption").GetComponent<Text> ().text = "Ammo: " + (totalbullets-shotsfired);
		GameObject.Find ("LivesCaption").GetComponent<Text> ().text = "Lives: " + lives;
		GameObject.Find ("ScoreCaption").GetComponent<Text> ().text = "Score: " + score;
	}


	void OnTriggerEnter2D(Collider2D otherObject)
	{
		if (otherObject.gameObject.tag == "ammoBox") {
			//reset bullets fired
			shotsfired = 0;
			//destroy the ammo box
			Destroy (otherObject.gameObject);
			UpdateText ();
		}
		if (otherObject.gameObject.tag == "alien") {
			if (lives > 0) {
				lives--;
				UpdateText ();
				Destroy (otherObject.gameObject);
			} else {
				//game over
				Destroy (this.gameObject);
			}

		}


	}
	
	// Update is called once per frame
	void Update () {

		//create a bullet every time I press space
		if (Input.GetKeyDown (KeyCode.Space)) {
			
			//shoot from 0.25 in front of the spaceship
			//note that we can add multiple vectors in this way
			if (shotsfired < totalbullets) {
				shotsfired++;
				Instantiate (bullet, transform.position + new Vector3 (0f, 0.25f), transform.rotation);
			}
			UpdateText ();
			//you're out of ammo!!
		}
	
		transform.Translate (new Vector3 (1f, 0f) * speed * Input.GetAxis ("Horizontal") * Time.deltaTime);

		transform.Translate (new Vector3 (0f, 1f) * speed * Input.GetAxis ("Vertical") * Time.deltaTime);


		transform.position = new Vector3 (
			Mathf.Clamp (transform.position.x, -limitx, limitx),
			Mathf.Clamp (transform.position.y, -limity, limity));

	}
}
