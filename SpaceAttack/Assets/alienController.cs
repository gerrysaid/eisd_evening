﻿using UnityEngine;
using System.Collections;

public class alienController : MonoBehaviour {


	spaceshipController playerController;

	// Use this for initialization
	void Start () {
		playerController = GameObject.Find ("spaceship").GetComponent<spaceshipController> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3 (0f, -1f) * 1f * Time.deltaTime);
		if (transform.position.y < -Camera.main.orthographicSize) {
			if (playerController.lives > 0) {
				playerController.lives--;
				playerController.UpdateText ();
			} else {
				Destroy (playerController.gameObject);
			}
			Destroy (this.gameObject);

		}
	}
}
