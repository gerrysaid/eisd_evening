﻿using UnityEngine;
using System.Collections;

public class bulletController : MonoBehaviour {

	spaceshipController playerController;


	// Use this for initialization
	void Start () {
		playerController = GameObject.Find ("spaceship").GetComponent<spaceshipController> ();
	}

	void OnTriggerEnter2D(Collider2D otherObject)
	{
		if (otherObject.gameObject.tag == "alien") {
			playerController.score++;
			playerController.UpdateText ();
			Destroy (otherObject.gameObject);
			Destroy (this.gameObject);

		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3 (0f, 1f) * 15f * Time.deltaTime);
		//if the bullet goes beyond the top of the screen, use the Destroy function to deallocate it and remove it from memory
		if (this.transform.position.y > Camera.main.orthographicSize) {
			Destroy (this.gameObject);
		}
	}
}
