﻿using UnityEngine;
using System.Collections;

public class physicsSquareController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		//up arrow pushes the box up
		if (Input.GetKeyDown (KeyCode.UpArrow)) 
		{
			GetComponent<Rigidbody2D> ().AddForce (new Vector3 (0f, 10f), ForceMode2D.Impulse);
		}


		if (Input.GetKeyDown (KeyCode.LeftArrow)) 
		{
			GetComponent<Rigidbody2D> ().AddForce (new Vector3 (-5f, 0f), ForceMode2D.Impulse);
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) 
		{
			GetComponent<Rigidbody2D> ().AddForce (new Vector3 (5f, 0f), ForceMode2D.Impulse);
		}


	}
}
