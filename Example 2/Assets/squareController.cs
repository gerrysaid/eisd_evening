﻿using UnityEngine;

//to access the text component
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class squareController : MonoBehaviour
{
	public float boxspeed = 10f;
	int score = 0;

	// Use this for initialization
	void Start ()
	{
	
	}

	//change box colour for a specified number of seconds
	IEnumerator changeBoxColor (int seconds)
	{
		GetComponent<SpriteRenderer> ().color = Color.red;
		yield return new WaitForSeconds (seconds);
		GetComponent<SpriteRenderer> ().color = Color.white;
		yield return null;
	}


	//collision code for a trigger
	void OnTriggerEnter2D (Collider2D otherObject)
	{
		//this is what is going to happen when I hit the circle
		if (otherObject.gameObject.tag == "circlePowerup") {
			Destroy (otherObject.gameObject);
			score++;
			GameObject.Find ("ScoreText").GetComponent<Text> ().text = score.ToString ();
			//when I hit the circle, the box needs to change colour for 2 seconds
			StartCoroutine (changeBoxColor (2));
		}

	}

	//this is the collision code for a collision
	void OnCollisionEnter2D (Collision2D hit)
	{
		if (hit.collider.gameObject.tag == "wall") {
			GetComponent<SpriteRenderer> ().color = Color.green;
		}


	}


	void OnCollisionExit2D (Collision2D hit)
	{
		GetComponent<SpriteRenderer> ().color = Color.white;
	}


	//go back to menu
	public void menuButtonPressed ()
	{
		SceneManager.LoadScene ("menu");
	}


	//this method is going to be called from the button, so it needs to be public
	public void resetSquarePosition ()
	{
		//since it is a 2d example, might as well assume z is 0 so we only use two parameters
		transform.position = new Vector3 (0f, 0f);
	}

	void moveSquare (float speed)
	{
		//time.deltaTime = how long did the PC take to draw one frame
		//horizontal movement
		transform.Translate (new Vector3 (1f, 0f) * speed * Input.GetAxis ("Horizontal") * Time.deltaTime); 

		//vertical movement
		transform.Translate (new Vector3 (0f, 1f) * speed * Input.GetAxis ("Vertical") * Time.deltaTime); 
	}

	
	// Update is called once per frame
	// Fixed update gives more time for the physics engine to work
	void FixedUpdate ()
	{
		moveSquare (boxspeed);
	}
}
