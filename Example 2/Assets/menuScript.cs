﻿using UnityEngine;
//to be able to switch between different scenes
using UnityEngine.SceneManagement;
using System.Collections;

public class menuScript : MonoBehaviour {


	public void TriggerCollisionsButtonPressed(){
		//the name of the scene is the parameter here
		SceneManager.LoadScene ("triggerCollisions");
	}

	public void ColliderCollisionsButtonPressed(){
		SceneManager.LoadScene ("colliderCollisions");
	}

	public void PhysicsSimulationButtonPressed(){
		SceneManager.LoadScene ("physicsCollisions");
	}

}
