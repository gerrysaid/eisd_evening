﻿using UnityEngine;
using System.Collections;

public class circleGenerationScript : MonoBehaviour {
	public GameObject myCircle;
	public int numberOfCircles=3;



	IEnumerator generateCircles(){
		//loop for number of circles times
		for (int i = 0; i < numberOfCircles; i++) {
			//random X value
			float randomX = Mathf.Round(Random.Range (-4.5f, 4.5f));
			//random Y value
			float randomY = Mathf.Round(Random.Range (-4.5f, 4.5f));

			//create a new instance of circle, at a random position, at 0 rotation
			Instantiate (myCircle, new Vector3 (randomX, randomY), Quaternion.identity);

			yield return new WaitForSeconds (1f);

		}
		yield return null;
	}


	// Use this for initialization
	void Start () {
		StartCoroutine (generateCircles ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
