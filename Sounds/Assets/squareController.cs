﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class squareController : MonoBehaviour {

	//reference to the sound to play
	public AudioClip soundToPlay;


	//reference to the audiosource in the camera
	AudioSource soundPlayer;
	string playerName;

	soundLibraryScript library;

	// Use this for initialization
	void Start () {

		playerName = PlayerPrefs.GetString ("name1");

		GameObject.Find ("PlayerNameText").GetComponent<Text> ().text = playerName;


		soundPlayer = Camera.main.GetComponent<AudioSource> ();

		library = GameObject.Find ("SoundLibrary").GetComponent<soundLibraryScript> ();


	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.forward * Input.GetAxis ("Horizontal") * 50f * Time.deltaTime);

		transform.Translate (Vector3.up * Input.GetAxis ("Vertical") * 20f * Time.deltaTime);

	}



	void OnTriggerEnter2D(Collider2D otherObject)
	{
		//play a sound when it hits anything

		soundPlayer.PlayOneShot (library.sounds[Random.Range(0,library.sounds.Length)]);
	}
}
