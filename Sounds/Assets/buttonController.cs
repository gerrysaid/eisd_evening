﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class buttonController : MonoBehaviour {


	public void changeSceneButtonPressed()
	{
		//the value in the input field
		string name = GameObject.Find ("NameInputField").GetComponent<InputField> ().text;
		//saves it in a text file to be passed between scenes
		PlayerPrefs.SetString ("name1", name);

		SceneManager.LoadScene ("soundscene");
	}

}
